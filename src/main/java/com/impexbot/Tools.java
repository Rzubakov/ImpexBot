/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.impexbot;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class Tools {

    public List<String> readPartsId(File file) {
        ArrayList<String> parts = new ArrayList<>();
        try {
            Scanner sc = new Scanner(file);
            sc.useDelimiter("\n");
            while (sc.hasNext()) {
                parts.add(sc.next());
            }
        } catch (FileNotFoundException ex) {
            System.out.println("File not found.");
            ex.printStackTrace();
        }
        System.out.println("Parts count: " + parts.size());
        return parts;
    }

    public void print(List<String> list) throws IOException {
        System.out.println("print result");
        PrintWriter out = new PrintWriter("result.csv");
        out.println("Производитель;Каталожный номер;Наим.(ENG);Наим.(RUS);Вес;Цена");
        list.forEach(line -> {
            out.println(line);
        });
        out.flush();
        out.close();
    }

    public List<String> getProxyList() {
        List<String> proxys = new ArrayList<>();
        try {
            Document doc = Jsoup.connect("http://spys.one/en/http-proxy-list/").data("xpp", "5").data("xf4", "2").post();

            doc.getElementsMatchingText(Pattern.compile(""
                    + "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
                    + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
                    + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
                    + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$")).forEach(e -> {
                        if (!proxys.contains(e.text())) {
                            proxys.add(e.text());
                        }
                    });
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        System.out.println("Proxys:" + proxys.size());
        return proxys;
    }

}
