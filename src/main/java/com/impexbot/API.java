/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.impexbot;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class API implements Callable<List<String>> {

    private int timeout; //default 2000 mc
    private List<String> proxys;
    private List<String> parts = new ArrayList<>();
    private int i = 0;
    private int err = 0;

    public API() {

    }

    public API(List<String> parts) {
        this.parts.addAll(parts);
    }

    public List<String> getResult() {
        ArrayList<String> result = new ArrayList<>();
        String query = null;
        System.out.println("Runing: " + Thread.currentThread().getName() + " loaded: " + parts.size());

        for (int i = 0; i < parts.size(); i++) {
            do {
                query = get(parts.get(i));
                if (query != null && !query.equals("not found")) {
                    result.add(query);
                    System.out.println(Thread.currentThread().getName() + " = " + query + "(" + i + ")");
                }
            } while (query == null);
        }

        System.out.println("End: " + Thread.currentThread().getName());
        return result;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public void setProxys(List<String> proxys) {
        this.proxys = proxys;
    }

    public List<String> getProxys() {
        return proxys;
    }

    public void setParts(List<String> parts) {
        this.parts.addAll(parts);
    }

    @Override
    public List<String> call() throws Exception {
        return getResult();
    }

    public List<String> getParts() {
        return parts;
    }

    private String get(String id) {

        String result = null;
        try {
            Document doc = Jsoup.connect("https://www.impex-jp.com/zip/part-search.html?part=" + id)
                    .userAgent("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36")
                    .referrer("https://www.impex-jp.com/")
                    .timeout(timeout)
                    .proxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxys.get(i), 8080)))
                    .get();
            if (doc.select(".parts-search-result-table tr").size() != 0) {
                result = doc.select(".parts-search-result-table tr").get(1).select("td").get(0).text() + "; "
                        + doc.select(".parts-search-result-table tr").get(1).select("td").get(1).text() + "; "
                        + doc.select(".parts-search-result-table tr").get(1).select("td").get(3).text() + "; "
                        + doc.select(".parts-search-result-table tr").get(1).select("td").get(4).text() + "; "
                        + doc.select(".parts-search-result-table tr").get(1).select("td").get(5).text() + "; "
                        + doc.select(".parts-search-result-table tr").get(1).select("td").get(6).select(".prim").text() + ";";
            } else {
                result = "not found";
            }
        } catch (IOException e) {

            if (i > proxys.size()) {
                i = 0;
            }
            if (err > 10) {
                System.out.println(Thread.currentThread().getName() + " Connection fail.");
                System.out.println("Change proxy");
                i++;
                err = 0;
            }
            err++;
        }
        return result;
    }

}
