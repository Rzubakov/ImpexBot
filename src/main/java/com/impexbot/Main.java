package com.impexbot;

import java.io.File;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {

    public static void main(String... args) throws Exception {
        Tools tools = new Tools();
        ExecutorService executor = Executors.newCachedThreadPool();
        ArrayList<API> apis = new ArrayList<>();
        ArrayList<String> parts = new ArrayList<>();
        ArrayList<String> result = new ArrayList<>();
        ArrayList<String> ps = new ArrayList<>();
        if (args.length < 3) {
            throw new Exception("Please check params");
        }

        System.out.println("timeout:" + args[0]);
        System.out.println("pause:" + args[1]);
        System.out.println("threads count:" + args[2]);

        tools.getProxyList();

        String path;

        while (true) {
            System.out.println("Start: " + Calendar.getInstance().getTime());
            try {
                path = new File("." + "\\parts_im_k1.in").getCanonicalPath().toString();
                parts.clear();
                parts.addAll(tools.readPartsId(new File(path)));
                Integer listSize = parts.size() / Integer.valueOf(args[2]);
                for (int i = 0; i < parts.size(); i += listSize) {
                    apis.add(new API(parts.subList(i, Math.min(i + listSize, parts.size()))));
                }

                apis.forEach(api -> {
                    api.setTimeout(Integer.valueOf(args[0]));
                });
                ps.addAll(tools.getProxyList());
                apis.forEach(api -> {
                    api.setProxys(ps);
                });

                System.out.println("Total threads: " + apis.size());
                for (Future<List<String>> f : executor.invokeAll(apis)) {
                    for (String s : f.get()) {
                        result.add(s);
                    }
                }
                tools.print(result);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            System.out.println("End: " + Calendar.getInstance().getTime());
            System.out.println("wait...");
            Thread.sleep(Long.valueOf(args[1]));
        }

    }

}
